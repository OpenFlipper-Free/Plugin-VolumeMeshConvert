/*===========================================================================*\
*                                                                            *
 *                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                            *
 \*===========================================================================*/


#pragma once

#include <OpenFlipper/BasePlugin/BaseInterface.hh>
#include <OpenFlipper/BasePlugin/LoggingInterface.hh>
#include <OpenFlipper/BasePlugin/ToolbarInterface.hh>
#include <OpenFlipper/BasePlugin/LoadSaveInterface.hh>
#include <OpenFlipper/BasePlugin/ScriptInterface.hh>

class VolumeMeshConvertPlugin: public QObject,
        BaseInterface,
        LoggingInterface,
        LoadSaveInterface,
        ScriptInterface,
        ToolbarInterface
{
    Q_OBJECT
    Q_INTERFACES(BaseInterface)
    Q_INTERFACES(LoggingInterface)
    Q_INTERFACES(ToolbarInterface)
    Q_INTERFACES(LoadSaveInterface)
    Q_INTERFACES(ScriptInterface)

    Q_PLUGIN_METADATA(IID "org.OpenFlipper.Plugins.Plugin-VolumeMeshConvert")

signals:

    // LoggingInterface
    void log(Logtype _type, QString _message) override;
    void log(QString _message) override;

    //ToolbarInterface
    void addToolbar(QToolBar* _toolbar) override;
    void getToolBar(QString _name, QToolBar*& _toolbar) override;

    // LoadSaveInterface
    void addEmptyObject( DataType _type, int& _id) override;

    // BaseInterface
    void updatedObject(int, const UpdateType& _type) override;

    // ScriptInterface
    void setSlotDescription(QString     _slotName,   QString     _slotDescription,
                            QStringList _parameters, QStringList _descriptions) override;

public:
    VolumeMeshConvertPlugin() = default;

    // BaseInterface
    QString name() override { return (QString("VolumeMeshConvert"));  }
    QString description() override { return (QString("Convert PolyhedralMesh to TetrahedralMesh and HexahedralMesh and vice versa")); }

private slots:

    /// BaseInterface
    void initializePlugin() override;
    void pluginsInitialized()override;

    /// Tell system that this plugin runs without ui
    void noguiSupported() override {}

private:

    QToolBar* toolbar_ = nullptr;
    QActionGroup* grp_ = nullptr;
    QAction* actionToHex_ = nullptr;
    QAction* actionToTet_ = nullptr;
    QAction* actionToPoly_ = nullptr;



private slots:
    /**
     * @brief convert Converts polyhedralmesh to hex/tet mesh and vice versa depending on the Action that was called.
     */
    void handleQAction(QAction*);

public slots:
    int convertToHexMesh(int _id);
    int convertToTetMesh(int _id);
    int convertToPolyhedralMesh(int _id);

    QString version() override { return QString("1.0"); }

};
