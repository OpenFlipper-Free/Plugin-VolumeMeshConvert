/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/





#include "VolumeMeshConvert.hh"
#include <OpenFlipper/common/GlobalOptions.hh>
#include <OpenFlipper/BasePlugin/PluginFunctions.hh>
#include <Type-OpenVolumeMesh/ObjectTypes/HexahedralMesh/PluginFunctionsHexahedralMesh.hh>
#include <Type-OpenVolumeMesh/ObjectTypes/TetrahedralMesh/PluginFunctionsTetrahedralMesh.hh>
#include <Type-OpenVolumeMesh/ObjectTypes/PolyhedralMesh/PluginFunctionsPolyhedralMesh.hh>
#include <QAction>
#include <QActionGroup>

void VolumeMeshConvertPlugin::initializePlugin()
{

}


void VolumeMeshConvertPlugin::pluginsInitialized()
{
  //populate scripting function
  emit setSlotDescription("convertToHexMesh(int)",
                          "Convert a polyhedral Mesh to a hexahedral mesh."
                          " Does not check if input mesh actually has the right topology."
                          " Return the ID of the new mesh or -1 in case of error."
                          " The old mesh remains unchanged.",
                          QStringList("object_id"),
                          QStringList("id of an the input object"));
  emit setSlotDescription("convertToTetMesh(int)",
                          "Convert a polyhedral Mesh to a tetrahedral mesh."
                          " Does not check if input mesh actually has the right topology."
                          " Return the ID of the new mesh or -1 in case of error."
                          " The old mesh remains unchanged.",
                          QStringList("object_id"),
                          QStringList("id of an the input object"));
  emit setSlotDescription("convertToPolyhedralMesh(int)",
                          "Convert a hex or tet mesh to a PolyhedralMesh."
                          " Return the ID of the new mesh or -1 in case of error."
                          " The old mesh remains unchanged.",
                          QStringList("object_id"),
                          QStringList("id of an the input object"));

  if(! OpenFlipper::Options::gui())
    return;

  // Create your toolbar
  toolbar_ = new QToolBar(tr("Volume Mesh conversion"));

  grp_ = new QActionGroup(toolbar_);

  // Create an action for the toolbar
  actionToHex_ = new QAction(tr("&Convert Polyhedral to Hexahedral mesh"), grp_);
  actionToTet_ = new QAction(tr("&Convert Polyhedral to Tetrahedral mesh"), grp_);
  actionToPoly_ = new QAction(tr("&Convert Tet or Hex mesh to Polyhedral mesh"), grp_);

  // Create an icon which is shown for the action
  // TODO
  actionToHex_->setIcon(QIcon(OpenFlipper::Options::iconDirStr()+OpenFlipper::Options::dirSeparator()+"VolumeConvertToHex.png"));
  actionToTet_->setIcon(QIcon(OpenFlipper::Options::iconDirStr()+OpenFlipper::Options::dirSeparator()+"VolumeConvertToTet.png"));
  actionToPoly_->setIcon(QIcon(OpenFlipper::Options::iconDirStr()+OpenFlipper::Options::dirSeparator()+"VolumeConvertToPoly.png"));

  // Add the action to the toolbar
  toolbar_->addAction(actionToHex_);
  toolbar_->addAction(actionToTet_);
  toolbar_->addAction(actionToPoly_);

  connect( grp_, SIGNAL( triggered(QAction*) ), this, SLOT(handleQAction(QAction*)) );

  // Integrate the new toolbar into OpenFlipper
  emit addToolbar( toolbar_ );
}

void VolumeMeshConvertPlugin::handleQAction(QAction* _action)
{
    std::vector<int> _ids;
    if(! PluginFunctions::getTargetIdentifiers( _ids  ))
        return;
    for(const auto id: _ids)
    {
        int out_id = -1;
        if (_action == actionToHex_)
            out_id = convertToHexMesh(id);
        else if (_action == actionToPoly_)
            out_id = convertToPolyhedralMesh(id);
        else if (_action == actionToTet_)
            out_id = convertToTetMesh(id);
        else {
            assert(false);
        }
        if (out_id == -1) {
            emit log(LOGERR, "VolumeMeshConvert: failed to convert object " + QString::number(id));
        } else {
            emit log(LOGINFO, "VolumeMeshConvert: successfully converted object"
                     + QString::number(id) + " to new object " + QString::number(out_id));
        }
    }
}

int VolumeMeshConvertPlugin::convertToHexMesh(int _id)
{
    PolyhedralMeshObject *pmo = PluginFunctions::polyhedralMeshObject(_id);
    if (!pmo)
        return -1;

    // TODO: here we should check that all cells are actually hexahedral

    int newID = -1;
    emit addEmptyObject(DATA_HEXAHEDRAL_MESH, newID);
    HexahedralMeshObject *hmo = PluginFunctions::hexahedralMeshObject(newID);

    *hmo->mesh() = *pmo->mesh();

    hmo->setName("Tet conv. of " + pmo->name());

    emit updatedObject(newID, UPDATE_ALL);
    return newID;
}

int VolumeMeshConvertPlugin::convertToTetMesh(int _id)
{
    PolyhedralMeshObject *pmo = PluginFunctions::polyhedralMeshObject(_id);
    if (!pmo)
        return -1;

    // TODO: here we should check that all cells are actually tetrahedral

    int newID = -1;
    emit addEmptyObject(DATA_TETRAHEDRAL_MESH, newID);
    TetrahedralMeshObject *tmo = PluginFunctions::tetrahedralMeshObject(newID);

    // TetMesh have their own TetrahedralGeometryKernel, which only adds
    // functionality, but should not influence assignment.
    // Downcast it to a GeometryKernel<Vec, TetTopoKernel>:
    auto *tm = static_cast<TetrahedralMesh::ParentT*>(tmo->mesh());

    *tm = *pmo->mesh();

    tmo->setName("Hex conv. of " + pmo->name());

    emit updatedObject(newID, UPDATE_ALL);
    return newID;
}

int VolumeMeshConvertPlugin::convertToPolyhedralMesh(int _id)
{
    HexahedralMeshObject *hmo = PluginFunctions::hexahedralMeshObject(_id);
    TetrahedralMeshObject *tmo = PluginFunctions::tetrahedralMeshObject(_id);
    if (!hmo && !tmo) {
        return -1;
    }
    int newID = -1;
    emit addEmptyObject(DATA_POLYHEDRAL_MESH, newID);
    PolyhedralMeshObject *pmo = PluginFunctions::polyhedralMeshObject(newID);
    if (hmo) {
        *pmo->mesh() = *hmo->mesh();
        pmo->setName("Polyhedral conv. of " + hmo->name());
    } else if (tmo) {
        auto *tm = static_cast<TetrahedralMesh::ParentT*>(tmo->mesh());
        *pmo->mesh() = *tm;
        pmo->setName("Polyhedral conv. of " + tmo->name());
    }

    emit updatedObject(newID, UPDATE_ALL);

    return newID;
}
